use nalgebra::{Pnt2, Vec2};

pub enum ParticleType {
    Sand,
}

impl ParticleType {
    pub fn color(&self) -> [f32; 4] {
        match *self {
            ParticleType::Sand => [1.0, 0.6, 0.1, 1.0],
        }
    }
}

pub struct Particle {
    pub position: Pnt2<f64>,
    pub velocity: Vec2<f64>,
    pub ty: ParticleType,
}

impl Particle {
    pub fn new(position: Pnt2<f64>, ty: ParticleType) -> Particle {
        Particle { position: position, velocity: Vec2::new(0.0, 0.0), ty: ty }
    }
}
