use std::rc::{Rc, Weak};
use nalgebra::sqdist;
use cfg::*;
use particle::Particle;

pub struct Particles {
    pub particles: Vec<Rc<Particle>>,
    pub links: Vec<(Weak<Particle>, Weak<Particle>)>,
}

impl Particles {
    pub fn new() -> Particles {
        Particles {
            particles: Vec::new(),
            links: Vec::new(),
        }
    }

    pub fn insert(&mut self, p: Particle) {
        self.particles.push(Rc::new(p));
    }

    pub fn step(&mut self) {
        self.links.clear();

        let mut piter = self.particles.iter();
        for p in piter.by_ref() {
            // using qiter here might be wrong because it might be permanently exhausted here
            let mut qiter = self.particles.iter();
            let qs = qiter.by_ref().filter(
                |q| sqdist(&p.position, &q.position) < CURSOR_RADIUS_SQUARED
                );
            for q in qs {
                self.links.push((p.downgrade(), q.downgrade()));
            }
        }
    }
}
