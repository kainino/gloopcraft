#![feature(box_syntax)]
#![feature(rc_weak)]

extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
extern crate nalgebra;

use piston::window::WindowSettings;
use piston::event::*;
use piston::input::{Input, Motion};
use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use graphics::Context;
use nalgebra::{Pnt2, sqdist};

use particle::{Particle, ParticleType};
use particles::{Particles};
use cfg::*;

mod cfg;
mod particle;
mod particles;


pub struct App {
    /// OpenGL drawing backend.
    gl: GlGraphics,
    /// Represents particle world.
    particles: Particles,
    mouse_left: bool,
    mouse_right: bool,
    mouse_current: Option<Pnt2<f64>>,
}

impl App {
    fn new(gl: GlGraphics) -> App {
        App {
            gl: gl,
            particles: Particles::new(),
            mouse_left: false,
            mouse_right: false,
            mouse_current: None,
        }
    }

    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        let particles = &self.particles;
        let mouse_current = &self.mouse_current;

        self.gl.draw(args.viewport(), |c, gl| {
            const COLOR_BACKGROUND: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
            clear(COLOR_BACKGROUND, gl);

            draw_particles(particles, c, gl);

            if let &Some(m) = mouse_current {
                draw_cursor(m, c, gl);
            }
        });
    }

    fn update(&mut self, _: &UpdateArgs) {
        self.particles.step();

        if let Some(m) = self.mouse_current {
            if self.mouse_left {
                self.particles.particles.retain(
                    |p| sqdist(&m, &p.position) > CURSOR_RADIUS_SQUARED
                );
            }
            if self.mouse_right {
                self.particles.insert(Particle::new(m, ParticleType::Sand))
            }
        }
    }

    fn input(&mut self, args: Input) {
        use piston::input::Input::*;
        use piston::input::Button::*;
        use piston::input::MouseButton::*;
        match args {
            Move(Motion::MouseCursor(x, y)) => {
                self.mouse_current = Some(Pnt2::new(x, y));
            },
            Press  (Mouse(Left )) => { self.mouse_left  = true; },
            Release(Mouse(Left )) => { self.mouse_left  = false; },
            Press  (Mouse(Right)) => { self.mouse_right = true; },
            Release(Mouse(Right)) => { self.mouse_right = false; },
            _ => {}
        }
    }
}

fn draw_particles(particles: &Particles, c: Context, gl: &mut GlGraphics) {
    use graphics::*;

    let square: graphics::types::Rectangle = rectangle::centered_square(0.0, 0.0, 1.0);

    const COLOR_LINK: [f32; 4] = [1.0, 1.0, 1.0, 0.5];
    for link in particles.links.iter() {
        let l = line::Line::new(COLOR_LINK, 1.0);
        let transform = c.transform;
        let &(ref p, ref q) = link;
        if let Some(p) = p.upgrade() {
            if let Some(q) = q.upgrade() {
                let points = [p.position.x, p.position.y, q.position.x, q.position.y];
                l.draw(points, default_draw_state(), transform, gl);
            }
        }
    }

    for p in particles.particles.iter() {
        let transform = c.transform.trans(p.position.x, p.position.y);
        ellipse(p.ty.color(), square, transform, gl);
    }
}

fn draw_cursor(m: Pnt2<f64>, c: Context, gl: &mut GlGraphics) {
    use graphics::*;

    let square: graphics::types::Rectangle = rectangle::centered_square(0.0, 0.0, CURSOR_RADIUS);

    const COLOR_CURSOR: [f32; 4] = [1.0, 1.0, 1.0, 0.2];
    let transform = c.transform.trans(m.x, m.y);
    ellipse(COLOR_CURSOR, square, transform, gl);
}

fn main() {
    let opengl = OpenGL::_3_2;

    // Create an Glutin window.
    let window = GlutinWindow::new(
        opengl,
        WindowSettings::new(
            "spinning-square",
            [200, 200]
        )
        .exit_on_esc(true)
    );

    // Create a new game and run it.
    let mut app = App::new(GlGraphics::new(opengl));

    for e in window.events() {
        match e {
            Event::Render(args) => app.render(&args),
            Event::Update(args) => app.update(&args),
            Event::Input(args) => app.input(args),
            _ => {}
        }
    }
}
